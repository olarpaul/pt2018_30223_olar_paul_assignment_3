package start;

import java.lang.reflect.Field;
import java.util.List;

import bll.ClientBLL;
import bll.OrderBLL;
import bll.ProductBLL;
import model.Client;
import model.Order;
import model.Product;

public class ReflectionUtils {

	public static String[] getHeaders(Object object) {
		String[] result = new String[object.getClass().getDeclaredFields().length];
		int i = 0;
		for (Field field : object.getClass().getDeclaredFields()) {
			field.setAccessible(true); // set modifier to public
			result[i] = field.getName();
			i++;
		}
		return result;
	}

	public static String[][] getMatrix(List<?> objects) {
		String[][] result = new String[objects.size()][4];
		for (int i = 0; i < objects.size(); i++) {
			Object object = objects.get(i);
			int j = 0;
			for (Field field : object.getClass().getDeclaredFields()) {
				field.setAccessible(true); // set modifier to public
				Object value;
				try {
					// Adaugam la rezultat informatiile corespunzatoare
					value = field.get(object);
					result[i][j] = String.valueOf(value);
					j++;
				} catch (IllegalArgumentException | IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		}
		return result;
	}
}